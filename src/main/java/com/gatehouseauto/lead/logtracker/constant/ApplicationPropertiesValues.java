package com.gatehouseauto.lead.logtracker.constant;

public class ApplicationPropertiesValues {
	public static String APPLICATION_MODE = "";
	
	public static String DB_URL = "";
	public static String DB_USR = "";
	public static String DB_PWD = "";
	
	public static String QUEUE_NAME = "";	
}
