package com.gatehouseauto.lead.logtracker.constant;

public interface ApplicationPropertiesKeys {
	
	public String APPLICATION_MODE = "application-mode";
	
	public String DB_URL_DEV = "db-url-dev";
	public String DB_USR_DEV = "db-usr-dev";
	public String DB_PWD_DEV = "db-pwd-dev";
	
	public String QUEUE_NAME_DEV = "queue-name-dev";	
	
	
	
	
	public String DB_URL_PROD = "db-url-prod";
	public String DB_USR_PROD = "db-usr-prod";
	public String DB_PWD_PROD = "db-pwd-prod";	
	
	public String QUEUE_NAME_PROD = "queue-name-prod";	
}
