package com.gatehouseauto.lead.logtracker.db.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;


@Repository(value = "leadLogTrackerDAO")
public class LeadLogTrackerDAOImpl extends GenericDAOImpl<com.gatehouseauto.lead.logtracker.db.bean.LeadLogTracker> implements LeadLogTrackerDAO{

	@Autowired
	@Qualifier(value="sessionFactory")
	private SessionFactory sessionFactory;
	
	public LeadLogTrackerDAOImpl(SessionFactory sessionFactory){}

	public LeadLogTrackerDAOImpl(){}
	
//    @SuppressWarnings("unchecked")
//	@Override
//	@Transactional(value="transactionManager")
//    public DealerVehicle getDealerVehicle(String fId, Integer vehicleId){
//		Criteria criteria = getSession().createCriteria(DealerVehicle.class);  
//		
//		criteria.add( Restrictions.eq("fid", fId) );
//		criteria.add( Restrictions.eq("vehicleId.vehicleId", vehicleId) );
//	
//		List<DealerVehicle> userVehicles =  criteria.list();
//		if(userVehicles == null || userVehicles.isEmpty()){
//			return null;
//		}		
//		return userVehicles.get(0);
//    }
	
    @SuppressWarnings("unused")
	private Session getSession(){
        if(sessionFactory.getCurrentSession() != null
                && sessionFactory.getCurrentSession().isOpen()) {
            return sessionFactory.getCurrentSession();
        } else {
            return sessionFactory.openSession();
        }
    }
	
}
