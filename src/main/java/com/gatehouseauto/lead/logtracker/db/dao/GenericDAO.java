package com.gatehouseauto.lead.logtracker.db.dao;

import java.util.List;

public interface GenericDAO<T> {
    public T save(T entity, boolean doFlush);
    public Boolean delete(T entity, boolean doFlush);
    public Boolean deleteList(List<T> entities, boolean doFlush);
    public boolean update(T entity, boolean doFlush);
    public T findByPrimaryKey(Integer entityId);
    public List<T> findListByPrimaryKeys(String propertyName, List<Integer> entityIds);
	public List<T> findWithPagination(String fieldNameEqual, String  fieldValueEqual, String valueDataType, String orderByField, 
			boolean ascending, 	int pageNo, int maxResult);
}
