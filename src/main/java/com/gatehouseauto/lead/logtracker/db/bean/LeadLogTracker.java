/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gatehouseauto.lead.logtracker.db.bean;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author basit
 */
@Entity
@Table(name = "lead_log_tracker")
@XmlRootElement
public class LeadLogTracker implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "lead_phone")
    private String leadPhone;
    @Size(max = 45)
    @Column(name = "lead_city")
    private String leadCity;
    @Size(max = 45)
    @Column(name = "lead_state")
    private String leadState;
    @Size(max = 45)
    @Column(name = "lead_zip")
    private String leadZip;
    @Size(max = 350)
    @Column(name = "lead_email")
    private String leadEmail;
    @Size(max = 200)
    @Column(name = "lead_comments")
    private String leadComments;
    @Size(max = 45)
    @Column(name = "vin")
    private String vin;
    @Size(max = 45)
    @Column(name = "year")
    private String year;
    @Size(max = 45)
    @Column(name = "make")
    private String make;
    @Size(max = 45)
    @Column(name = "model")
    private String model;
    @Size(max = 45)
    @Column(name = "trim")
    private String trim;
    @Size(max = 45)
    @Column(name = "vehicle_status")
    private String vehicleStatus;
    @Size(max = 45)
    @Column(name = "stock_no")
    private String stockNo;
    @Size(max = 45)
    @Column(name = "vendor_id")
    private String vendorId;
    @Size(max = 150)
    @Column(name = "vendor_name")
    private String vendorName;
    @Size(max = 200)
    @Column(name = "provider")
    private String provider;
    @Size(max = 45)
    @Column(name = "request_date")
    private String requestDate;
    @Lob
    @Column(name = "adf_xml")
    private byte[] adfXml;
    @Size(max = 200)
    @Column(name = "error_message")
    private String errorMessage;
    @Basic(optional = false)
    @NotNull
    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    public LeadLogTracker() {
    }

    public LeadLogTracker(Integer id) {
        this.id = id;
    }

    public LeadLogTracker(Integer id, Date createdOn) {
        this.id = id;
        this.createdOn = createdOn;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLeadPhone() {
        return leadPhone;
    }

    public void setLeadPhone(String leadPhone) {
        this.leadPhone = leadPhone;
    }

    public String getLeadCity() {
        return leadCity;
    }

    public void setLeadCity(String leadCity) {
        this.leadCity = leadCity;
    }

    public String getLeadState() {
        return leadState;
    }

    public void setLeadState(String leadState) {
        this.leadState = leadState;
    }

    public String getLeadZip() {
        return leadZip;
    }

    public void setLeadZip(String leadZip) {
        this.leadZip = leadZip;
    }

    public String getLeadEmail() {
        return leadEmail;
    }

    public void setLeadEmail(String leadEmail) {
        this.leadEmail = leadEmail;
    }

    public String getLeadComments() {
        return leadComments;
    }

    public void setLeadComments(String leadComments) {
        this.leadComments = leadComments;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getTrim() {
        return trim;
    }

    public void setTrim(String trim) {
        this.trim = trim;
    }

    public String getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(String vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public String getStockNo() {
        return stockNo;
    }

    public void setStockNo(String stockNo) {
        this.stockNo = stockNo;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public byte[] getAdfXml() {
        return adfXml;
    }

    public void setAdfXml(byte[] adfXml) {
        this.adfXml = adfXml;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LeadLogTracker)) {
            return false;
        }
        LeadLogTracker other = (LeadLogTracker) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.gatehouseauto.lead.logtracker.db.bean.LeadLogTracker[ id=" + id + " ]";
    }
    
}
