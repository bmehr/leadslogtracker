package com.gatehouseauto.lead.logtracker.db.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

import com.gatehouseauto.lead.logtracker.utils.CommonUtils;
import com.gatehouseauto.lead.logtracker.utils.LoggingUtils;



public class GenericDAOImpl <T> implements GenericDAO<T>{
	
	@Autowired
	@Qualifier(value="sessionFactory")
	private SessionFactory sessionFactory;
	
	protected Class<T> entityClass;
	
	public GenericDAOImpl(SessionFactory sessionFactory){
		this();
	}

	@SuppressWarnings("unchecked")
	public GenericDAOImpl(){
        ParameterizedType genericSuperclass = (ParameterizedType) getClass()
                .getGenericSuperclass();
           this.entityClass = (Class<T>) genericSuperclass
                .getActualTypeArguments()[0];		
	}
	
	@Override
	@Transactional(value="transactionManager")
	public T save(T entity, boolean doFlush) {
		try{
			Session hibernateSession = getSession();
			if(doFlush){
				hibernateSession.setFlushMode(FlushMode.MANUAL);
			}
			
			hibernateSession.save(entity);
			
			if(doFlush){
				hibernateSession.flush();
				hibernateSession.clear();
			}
		}catch(Exception e){
			LoggingUtils.logDebug(LoggingUtils.getExceptionString(e));
			e.printStackTrace();
//			throw new RuntimeException("Rolling back transaction!");
		}
		
		return entity;	
	}

	@Override
	@Transactional(value="transactionManager")
	public Boolean delete(T entity, boolean doFlush) {
		try{
			Session session = getSession();
			session.delete(entity);
		    
			if(doFlush){
				session.flush();
				session.clear();
			}
		}catch(Exception e){
			LoggingUtils.logDebug(LoggingUtils.getExceptionString(e));
//			throw new RuntimeException("Rolling back transaction!");
		}
	    return true;	   
	}
	
	@Override
	@Transactional(value="transactionManager")
	public Boolean deleteList(List<T> entities, boolean doFlush) {
		try{
			Session session = getSession();
			entities.forEach(entity ->{
				session.delete(entity);
			});
		    
			if(doFlush){
				session.flush();
				session.clear();
			}
		}catch(Exception e){
			LoggingUtils.logDebug(LoggingUtils.getExceptionString(e));
//			throw new RuntimeException("Rolling back transaction!");
		}
	    return true;	   
	}

	@Override
	@Transactional(value="transactionManager")
	public boolean update(T entity, boolean doFlush) {
		try{
			Session session = getSession();
			
			session.update(entity);
			
			if(doFlush){
				session.flush();
				session.clear();
			}
		}catch(Exception e){
			LoggingUtils.logDebug(LoggingUtils.getExceptionString(e));
//			throw new RuntimeException("Rolling back transaction!");
		}
	    return true;	
	}

	@Override
	@Transactional(value="transactionManager")
	public T findByPrimaryKey(Integer entityId) {
		return getSession().get(entityClass, entityId);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(value="transactionManager")
	public List<T> findListByPrimaryKeys(String propertyName, List<Integer> entityIds) {
		Criteria criteria = getSession().createCriteria(entityClass);  
		criteria.add( Restrictions.in(propertyName, entityIds));
		
		return criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional(value="transactionManager")
	public List<T> findWithPagination(String fieldNameEqual, String  fieldValueEqual, String valueDataType, String orderByField, 
			boolean ascending, 	int pageNo, int maxResult) {
		
		if(maxResult == 0){
			maxResult = 10;
		}
		
		int startFrom = 0;
		if(pageNo != 0){
			startFrom =  pageNo * maxResult;
		}

		Criteria criteria = getSession().createCriteria(entityClass);  
		
		if(fieldNameEqual != null){
			if(valueDataType != null){
				if(valueDataType.contains("int")){
					int fieldValueEqualInt = CommonUtils.parseIntString(fieldValueEqual);
					criteria.add( Restrictions.eq(fieldNameEqual, fieldValueEqualInt) );
				}else if(valueDataType.contains("double") || valueDataType.contains("float")){
					double fieldValueEqualDouble = CommonUtils.parseDoubleString(fieldValueEqual);
					criteria.add( Restrictions.eq(fieldNameEqual, fieldValueEqualDouble) );
				}else{
					//string
					criteria.add( Restrictions.eq(fieldNameEqual, fieldValueEqual) );
				}
			}
		}
		
		if(orderByField != null && orderByField.trim().length() > 0){
			if(ascending){
				criteria.addOrder(Property.forName(orderByField).asc());
			}else{
				criteria.addOrder(Property.forName(orderByField).desc());
			}
		}
		
		criteria.setFirstResult(startFrom);
		criteria.setMaxResults(maxResult);
		
		return criteria.list();
	}

	
    private Session getSession(){
        if(sessionFactory.getCurrentSession() != null
                && sessionFactory.getCurrentSession().isOpen()) {
            return sessionFactory.getCurrentSession();
        } else {
            return sessionFactory.openSession();
        }
    }

}
