package com.gatehouseauto.lead.logtracker.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Vendor {
//	private Id id;
	
	private String id;

	private String vendorname;

//	public Id getId() {
//		return id;
//	}
//
//	public void setId(Id id) {
//		this.id = id;
//	}

	public String getVendorname() {
		return vendorname;
	}

	public void setVendorname(String vendorname) {
		this.vendorname = vendorname;
	}

	@Override
	public String toString() {
		return "ClassPojo [id = " + id + ", vendorname = " + vendorname + "]";
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
