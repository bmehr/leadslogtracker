package com.gatehouseauto.lead.logtracker.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Adf {
	private Prospect prospect;

	public Prospect getProspect() {
		return prospect;
	}

	public void setProspect(Prospect prospect) {
		this.prospect = prospect;
	}

	@Override
	public String toString() {
		return "ClassPojo [prospect = " + prospect + "]";
	}
}