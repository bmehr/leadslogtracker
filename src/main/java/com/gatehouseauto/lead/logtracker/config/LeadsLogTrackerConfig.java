package com.gatehouseauto.lead.logtracker.config;

import java.sql.Connection;
import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.gatehouseauto.lead.logtracker.constant.ApplicationPropertiesValues;
import com.gatehouseauto.lead.logtracker.db.bean.LeadLogTracker;

@Configuration
@EnableAutoConfiguration
@EnableTransactionManagement
@ComponentScan(basePackages = "com.gatehouseauto")
public class LeadsLogTrackerConfig {
	  
	  @Bean(name = "dataSource")
	  public DataSource getDataSourceBestride() {
	    BasicDataSource  dataSource = new BasicDataSource();
	    try{
	      dataSource.setDriverClassName("com.mysql.jdbc.Driver");
	      dataSource.setUrl(ApplicationPropertiesValues.DB_URL);
	      dataSource.setUsername(ApplicationPropertiesValues.DB_USR);
	      dataSource.setPassword(ApplicationPropertiesValues.DB_PWD);
	      dataSource.setMaxIdle(10);
	      dataSource.setInitialSize(5);
	      dataSource.setMaxTotal(80);
	      dataSource.setMinIdle(5);
	      dataSource.setMaxWaitMillis(2000);
	      dataSource.setTestOnBorrow(true);
	      }catch (Throwable t) {
	          t.printStackTrace();
	      }    
	      return dataSource;
	  }
	  

	    @Autowired
	    @Qualifier(value="dataSource")
	    @Bean(name = "sessionFactory")
	    public SessionFactory getSessionFactory(DataSource dataSource) {
	        LocalSessionFactoryBuilder sessionBuilder = new LocalSessionFactoryBuilder(dataSource);
	        sessionBuilder.addProperties(getHibernateProperties());
	        sessionBuilder.addAnnotatedClasses(LeadLogTracker.class);
	        
	        return sessionBuilder.buildSessionFactory();
	    }
	  
	   
	    @Autowired
	    @Qualifier(value="sessionFactory")
	    @Bean(name = "transactionManager")
	    public HibernateTransactionManager getTransactionManager(
	            SessionFactory sessionFactory) {
	        HibernateTransactionManager transactionManager = new HibernateTransactionManager(
	                sessionFactory);
	    
	        return transactionManager;
	    }

	    private Properties getHibernateProperties() {
	        Properties properties = new Properties();
	        
	        properties.put("hibernate.show_sql", "false");
	        properties.put("hibernate.format_sql", "false");
	        properties.put("hibernate.use_sql_comments", "false");
	        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
	        properties.put("spring.jpa.database-platform", "org.hibernate.dialect.MySQLDialect");
	        properties.put("log4j.logger.org.hibernate", "fatal");
//	      properties.put("hibernate.enable_lazy_load_no_trans", "true"); //only if lazy=true
	        //TODO: To configure previous value . please read some article to tune hibernate query - 
//	      properties.put("hibernate.connection.release_mode", "after_statement");
	        
	        //isolation level
	        properties.setProperty("hibernate.connection.isolation", String.valueOf(Connection.TRANSACTION_READ_COMMITTED));    
	        
	        
	        return properties;
	    }

}
