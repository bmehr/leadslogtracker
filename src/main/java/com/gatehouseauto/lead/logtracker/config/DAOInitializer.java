package com.gatehouseauto.lead.logtracker.config;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import com.gatehouseauto.lead.logtracker.db.dao.LeadLogTrackerDAO;
import com.gatehouseauto.lead.logtracker.utils.LoggingUtils;


public class DAOInitializer {
	
	private static  ApplicationContext ctx = null;
	private static  LeadLogTrackerDAO leadLogTrackerDAO = null;
	
	private static void initialize(){
		try{
			if(ctx == null){
				ctx = new AnnotationConfigApplicationContext(LeadsLogTrackerConfig.class);	
			}
			
			if(leadLogTrackerDAO == null){
				leadLogTrackerDAO = (LeadLogTrackerDAO)ctx.getBean("leadLogTrackerDAO");
			}
		
		}catch(Exception e){
			e.printStackTrace();
			LoggingUtils.logDebug(LoggingUtils.getExceptionString(e));
		}
	}
	
	public static LeadLogTrackerDAO getLeadLogTrackerDAO() {
		if(leadLogTrackerDAO == null){
			initialize();
		}
		return leadLogTrackerDAO;
	}
	
	
	public static ApplicationContext getCtx() {
		if(ctx == null){
			initialize();
		}
		return ctx;
	}

}
