package com.gatehouseauto.lead.logtracker;

import org.springframework.context.ApplicationContext;

import com.gatehouseauto.lead.logtracker.config.DAOInitializer;
import com.gatehouseauto.lead.logtracker.sqs.SQSHandler;
import com.gatehouseauto.lead.logtracker.utils.LeadsLogTrackerUtils;

public class LeadsLogTrackerMain {

	public static void main(String[] args) {
		try{			
			LeadsLogTrackerUtils.loadProperties();
			
			ApplicationContext applicationContext = DAOInitializer.getCtx();
			
			System.out.println("applicationContext................... :"+ applicationContext);
			
			
			SQSHandler.startListener();
		}catch(Exception e){
			e.printStackTrace();
		}	

	}
}
