package com.gatehouseauto.lead.logtracker.sqs;

import java.util.Date;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.gatehouseauto.lead.logtracker.bean.Address;
import com.gatehouseauto.lead.logtracker.bean.Adf;
import com.gatehouseauto.lead.logtracker.bean.Contact;
import com.gatehouseauto.lead.logtracker.bean.Vehicle;
import com.gatehouseauto.lead.logtracker.bean.Vendor;
import com.gatehouseauto.lead.logtracker.config.DAOInitializer;
import com.gatehouseauto.lead.logtracker.db.bean.LeadLogTracker;
import com.gatehouseauto.lead.logtracker.db.dao.LeadLogTrackerDAO;
import com.gatehouseauto.lead.logtracker.parser.ADFParser;
import com.gatehouseauto.lead.logtracker.utils.LoggingUtils;

public class SQSListener implements MessageListener {
	
	private ADFParser adfParser = new ADFParser();
	
	LeadLogTrackerDAO leadLogTrackerDAO = DAOInitializer.getLeadLogTrackerDAO();
	
    @Override
    public void onMessage(Message message) {

    	String adfXML = null;
        try {            
        	adfXML = ((TextMessage) message).getText();
            
        	LoggingUtils.logInfo("_______________ \n"+ adfXML);
        	
            if(adfXML != null){
            	adfXML = adfXML.replaceAll("&", "&amp;");
            	adfXML = adfXML.replaceAll(">\\s+<", "><");
            	
            	Adf adf = adfParser.parse(adfXML);
            	
            	LeadLogTracker leadLogTracker = new LeadLogTracker();
            	if(adf == null || 
            			adf.getProspect() == null || 
            			adf.getProspect().getCustomer() == null || 
            			adf.getProspect().getCustomer().getContact() == null ||
            			adf.getProspect().getVehicle() == null ||
            			adf.getProspect().getVendor() == null){ 
            		
        			leadLogTracker.setErrorMessage("UNABLE_TO_PARSED_XML");
        			leadLogTracker.setAdfXml(adfXML.getBytes());  
            	}else{ 
            		
            		Contact contact = adf.getProspect().getCustomer().getContact();
            		
            		leadLogTracker.setLeadPhone(contact.getPhone());
            		leadLogTracker.setLeadEmail(contact.getEmail());
            		Address address = contact.getAddress();
            		if(address != null){
            			leadLogTracker.setLeadCity(address.getCity());
            			leadLogTracker.setLeadState(address.getRegioncode());
            			leadLogTracker.setLeadZip(address.getPostalcode());
            		}
            		
            		String leadComments = adf.getProspect().getCustomer().getComments();
            		if(leadComments != null && leadComments.length() > 300){
            			leadLogTracker.setLeadComments(leadComments.substring(0, 298));
            		}else{
            			leadLogTracker.setLeadComments(leadComments);
            		}
            		
            		
            		//Vehicles
            		Vehicle vehicle = adf.getProspect().getVehicle();
            		
            		leadLogTracker.setVin(vehicle.getVin());
            		leadLogTracker.setYear(vehicle.getYear());
            		leadLogTracker.setMake(vehicle.getMake());
            		leadLogTracker.setModel(vehicle.getModel());
            		leadLogTracker.setTrim(vehicle.getTrim());
            		leadLogTracker.setVehicleStatus(vehicle.getStatus());
            		leadLogTracker.setStockNo(vehicle.getStock());
            		
            		//Vendor
            		Vendor vendor = adf.getProspect().getVendor();
            		leadLogTracker.setVendorId(vendor.getId()+"");
            		leadLogTracker.setVendorName(vendor.getVendorname());
            		
            		leadLogTracker.setRequestDate(adf.getProspect().getRequestdate());            		
            		
            	}
            	
            	leadLogTracker.setCreatedOn(new Date());
            	leadLogTrackerDAO.save(leadLogTracker, true);            	
            }
	 
        } catch (JMSException e) {
        	LoggingUtils.logDebug(LoggingUtils.getExceptionString(e));
        }
    }

}
