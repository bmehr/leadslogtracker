package com.gatehouseauto.lead.logtracker.sqs;

import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.gatehouseauto.lead.logtracker.constant.ApplicationPropertiesValues;
import com.gatehouseauto.lead.logtracker.utils.LoggingUtils;

public class SQSHandler {
	
	public static void startListener(){
		Queue queue = null;
		Session session = null;
		try{
			BasicAWSCredentials awsCredentials = new BasicAWSCredentials("AKIAJ3L5275EYWNNWHKQ", "xOEBuxLDfaC3sTG8+vsucE9WtW32W1mSZpnV4R1M");
			SQSConnectionFactory connectionFactory =
					SQSConnectionFactory.builder()
				        .withRegion(Region.getRegion(Regions.US_EAST_1))
				        .build();
			
			SQSConnection sqsConnection = connectionFactory.createConnection(awsCredentials);
			
			session = sqsConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
	
			
			queue = session.createQueue(ApplicationPropertiesValues.QUEUE_NAME);
			
			MessageConsumer consumer = session.createConsumer(queue);
			
			// Instantiate and set the message listener for the consumer.
			consumer.setMessageListener(new SQSListener());
			 
			sqsConnection.start();
			
			System.out.println("..................SQS Listerner Started.......................................... : "+ ApplicationPropertiesValues.QUEUE_NAME);
			
			// Wait for 1 second. The listener onMessage() method will be invoked when a message is received.
//			Thread.sleep(1000);
		}catch(Exception e){
			LoggingUtils.logDebug(LoggingUtils.getExceptionString(e));
		}
	}

}
