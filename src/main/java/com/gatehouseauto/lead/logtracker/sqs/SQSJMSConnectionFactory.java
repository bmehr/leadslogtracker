package com.gatehouseauto.lead.logtracker.sqs;

import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.gatehouseauto.lead.logtracker.constant.ApplicationPropertiesValues;

public class SQSJMSConnectionFactory {
	private static volatile Session session = null;	
	private static volatile MessageConsumer consumer = null;
	
	private SQSJMSConnectionFactory(){}
	
	public static Session getSQSMessagingConnectionSession(){
		try{
			if((session == null  ) && !"".equals("AKIAJ3L5275EYWNNWHKQ")){
				synchronized (Session.class) {
					// Double check
					if (session == null && !"".equals("AKIAJ3L5275EYWNNWHKQ")) {
						BasicAWSCredentials awsCredentials = new BasicAWSCredentials("AKIAJ3L5275EYWNNWHKQ", "xOEBuxLDfaC3sTG8+vsucE9WtW32W1mSZpnV4R1M");
						SQSConnectionFactory connectionFactory =
								SQSConnectionFactory.builder()
							        .withRegion(Region.getRegion(Regions.US_EAST_1))
							        .build();
	
						SQSConnection sqsConnection = connectionFactory.createConnection(awsCredentials);
						session = sqsConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			session = null;
		}
		
		return session;
	}
	
	public static MessageConsumer getMessageConsumer(){
		try{
			if(consumer == null){
				synchronized (MessageConsumer.class) {
					// Double check
					if (consumer == null) {
						if(session == null){
							getSQSMessagingConnectionSession();
						}
						Queue queue = session.createQueue(ApplicationPropertiesValues.QUEUE_NAME);
						consumer = session.createConsumer(queue);

					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			consumer = null;
		}
		
		return consumer;
	}
}
