package com.gatehouseauto.lead.logtracker.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.gatehouseauto.lead.logtracker.constant.LogTrackerConstants;

public class CommonUtils {
	public static double parseDoubleString(String doubleString){
		double returnValue = 0.0;                             
		try{
			returnValue = new Double(doubleString.trim());
		}catch(Exception e){}
		return returnValue;
	}
	
	public static float parseDoubleToFloat(Double doubleValue){
		float returnValue = 0.0f;
		try{
			returnValue = doubleValue.floatValue();
		}catch(Exception e){}
		return returnValue;
	}
	
	public static long parseLongString(String longString){
		long returnValue = 0;
		try{
			returnValue = new Long(longString.trim());
		}catch(Exception e){}
		return returnValue;
	}
	
	public static byte parseByteString(String byteString){
		byte returnValue = 0;
		try{
			returnValue = Byte.valueOf(byteString);
		}catch(Exception e){}
		return returnValue;
	}
	
	public static boolean parseBooleanString(String booleanString){
		boolean returnValue = false;
		try{
			int parsedInt = parseIntString(booleanString);
			if(parsedInt == 1){
				return true;
			}
			
			returnValue = new Boolean(booleanString.trim());
		}catch(Exception e){}
		return returnValue;
	}
	
	public static int parseIntString(String intString){
		int returnValue = 0;
		try{
			returnValue = new Integer(intString.trim());
		}catch(Exception e){}
		return returnValue;
	}
	
	public static int parseFloatStringToInt(String intString){
		int returnValue = 0;
		try{
			returnValue = new Float(intString.trim()).intValue();
		}catch(Exception e){}
		return returnValue;
	}
	
	public static int parseIntegerObject(Object integerObject){
		int returnValue = 0;
		try{
			returnValue = (Integer)integerObject;
		}catch(Exception e){}
		return returnValue;
	}
	
	public static short parseShortObject(Object shortObject){
		Short returnValue = 0;
		try{
			returnValue = Short.parseShort(shortObject+"");
		}catch(Exception e){}
		return returnValue;
	}

	public static float parseFloatObject(Object floatObject){
		float returnValue = 0.0f;
		try{
			returnValue = (Float)floatObject;
		}catch(Exception e){}
		return returnValue;
	}
	
	public static Date parseDateObject(Object dateObject){
		Date returnValue = null;
		try{
			returnValue = (Date)dateObject;
		}catch(Exception e){}
		return returnValue;
	}
	
	public static double parseDoubleObject(Object integerObject){
		double returnValue = 0.0;
		try{
			returnValue = (double)integerObject;
		}catch(Exception e){}
		return returnValue;
	}
		
	public Date toDate(String dateString, String format){
		Date resultDate = null;
		try{
			SimpleDateFormat formatter = new SimpleDateFormat(format);
			synchronized(formatter){
				resultDate = formatter.parse(dateString);
				return resultDate;
			}
	 
		}catch(Exception e){}
		
		return resultDate;
	}
	
	public Calendar convertToCalendar(String calendarString){
		Calendar calendar = null;
		try{
			ZonedDateTime zonedDateTime = ZonedDateTime.parse(calendarString);
			
			String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			synchronized(simpleDateFormat){
				TimeZone timeZone = TimeZone.getTimeZone(zonedDateTime.getZone());
				simpleDateFormat.setTimeZone(timeZone);
				Date date = simpleDateFormat.parse(calendarString);
				
				calendar = Calendar.getInstance();
				calendar.setTime(date);
				calendar.setTimeZone(timeZone);
				
				return calendar;
			}
		}catch(Exception e){}
		
		return calendar;
	}
	
	public String convertMilliToDateStr(long milli){
		String result = "";
		try{
			long timeDifference = milli/1000;
			int h = (int) (timeDifference / (3600));
			int m = (int) ((timeDifference - (h * 3600)) / 60);
			int s = (int) (timeDifference - (h * 3600) - m * 60);

			result = String.format("%02d:%02d:%02d", h,m,s);
		}catch(Exception e){}
		return result;
	}
	
	public XMLGregorianCalendar getXMLGregorianCalendar(Date date){
		XMLGregorianCalendar calendar = null;
		try{
			GregorianCalendar gregory = new GregorianCalendar();
			gregory.setTime(date);
	
			calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
		}catch(Exception e){
			e.printStackTrace();
		}
		return calendar;
	}
	
	public Calendar getCalendar(Date date){
		Calendar calendar = null;
		try{
			calendar = Calendar.getInstance(); 
	
			calendar.setTimeInMillis(date.getTime());
		}catch(Exception e){
			e.printStackTrace();
		}
		return calendar;
	}
	
	public boolean isSameDayDate(Date nowDate, Date otherDate){
		boolean sameDay = false;
		try{
			Calendar cal1 = Calendar.getInstance();
			Calendar cal2 = Calendar.getInstance();
			cal1.setTime(nowDate);
			cal2.setTime(otherDate);
			sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
			                  cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);		
			
		
		}catch(Exception e){}
		return sameDay;
	}
	
	public Date parseDateString(String dateStr, String format, String timeZoneId){
		String timeZoneInside = "";
		if(timeZoneId == null || "".equalsIgnoreCase(timeZoneId)){
			timeZoneInside = LogTrackerConstants.DEFAULT_TIME_ZONE;
		}else{
			timeZoneInside = timeZoneId;
		}
		
		try{
			DateFormat dateFormat = new SimpleDateFormat(format);//"MM-dd-yyyy HH:mm:ss.SSSSZ"
			synchronized(dateFormat){
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneInside)); //America/New_York ,   America/North_Dakota/Center
				return dateFormat.parse(dateStr);
			}
		}catch(Exception e){}
		
		return null;
	}
	
	public String dateToString(Date dateParam, String dateFormatStr){
		String result = "";
		try{
			DateFormat dateFormat = new SimpleDateFormat(dateFormatStr);
			synchronized(dateFormat){
				result =  dateFormat.format(dateParam);
				return result;
			}
			
		}catch(Exception e){}
		return result;
	}

	public String dateToString(Date date, String format, String timeZoneId){
		String result = null;
		try{
			DateFormat dateFormat = new SimpleDateFormat(format);
			synchronized(dateFormat){
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneId));
				result = dateFormat.format(date);	
				return result;
			}
		}catch(Exception e){}
		
		return result;
	}

	
	public float convertMilliToMinutes(long millis){
		float minutes = 0.0f;
		try{
			float seconds=(millis/1000)%60;
			minutes=((millis-seconds)/1000)/60;
		}catch(Exception e){}
		return minutes;
	}
	
	public double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.HALF_UP);
	    return bd.doubleValue();
	}
	
	public long getDifferenceTimeInMilli(long startTime, long endTime){
		if(startTime == 0){
			return 0;
		}
		
		long differenceTime = ChronoUnit.MILLIS.between(Instant.ofEpochMilli(startTime) , Instant.ofEpochMilli(endTime));
		return differenceTime;
	}
	
	public long getDifferenceTimeInSeconds(long startTime, long endTime){
		if(startTime == 0){
			return 0;
		}
		
		long differenceTime = ChronoUnit.SECONDS.between(Instant.ofEpochMilli(startTime) , Instant.ofEpochMilli(endTime));
		return differenceTime;
	}
	
	public static long getDifferenceTimeInHours(long startTime, long endTime){
		if(startTime == 0){
			return 0;
		}
		
		long differenceTime = ChronoUnit.HOURS.between(Instant.ofEpochMilli(startTime) , Instant.ofEpochMilli(endTime));
		return differenceTime;
	}
	
	public static long getDifferenceTimeInMinutes(long startTime, long endTime){
	   if(startTime == 0){
	        return 0;
	   }
	        
	   long differenceTime = ChronoUnit.MINUTES.between(Instant.ofEpochMilli(startTime) , Instant.ofEpochMilli(endTime));
	   return differenceTime;
	}

	
	public String convertHexToString(String hex){
        StringBuilder responseSb = new StringBuilder();

        //49204c6f7665204a617661 split into two characters 49, 20, 4c...
        for( int i=0; i<hex.length()-1; i+=2 ){

            //grab the hex in pairs
            String output = hex.substring(i, (i + 2));
            //convert hex to decimal
            int decimal = Integer.parseInt(output, 16);
            //convert the decimal to character
            responseSb.append((char)decimal);
        }

        return responseSb.toString();
    }
	
	public static String getCacheKeyForSet(String groupName, String hexaModeId, String pidHexaId){			
		return new StringBuilder(groupName)
			.append("_")
			.append(hexaModeId)
			.append(pidHexaId).toString();
	}
	
	
    public static String hexToBinary(String hexStr) {
        String toReturn = new BigInteger(hexStr, 16).toString(2);
        return String.format("%" + (hexStr.length()*4) + "s", toReturn).replace(' ', '0');
    }
    
    public static String randomToken(int length){
    	String characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz@#$%&-";
    	SecureRandom secureRandom = new SecureRandom();
    	
       StringBuilder sb = new StringBuilder(length);
	   for( int i = 0; i < length; i++ ){
	      sb.append( characters.charAt(secureRandom.nextInt(characters.length())));
	   }
	   return sb.toString();
    }
}
