package com.gatehouseauto.lead.logtracker.utils;

import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

import com.gatehouseauto.lead.logtracker.constant.ApplicationPropertiesKeys;
import com.gatehouseauto.lead.logtracker.constant.ApplicationPropertiesValues;


public class LeadsLogTrackerUtils {
	
	public static void loadProperties(){
		
		String propertyFileNameDev = "";
		String propertyFileNameProd = "";
		
		try{			
    		propertyFileNameDev = "c://temp//leadslogtracker.properties";
    		propertyFileNameProd = "/opt/leadslogtracker/leadslogtracker.properties";
			Properties properties = new Properties();
			   
			// If file exists as an absolute path, load as input stream.
			final Path pathProd = Paths.get(propertyFileNameProd);
			
			if (Files.exists(pathProd, LinkOption.NOFOLLOW_LINKS)) {		//If Production
				
				   properties.load(new FileInputStream(propertyFileNameProd));				    
			 } else {		// else if dev or local
				 
			     properties.load(new FileInputStream(propertyFileNameDev));				      
			 } 
			
			ApplicationPropertiesValues.APPLICATION_MODE =  properties.getProperty(ApplicationPropertiesKeys.APPLICATION_MODE);
			 
			System.out.println("..................APPLICATION_MODE..........................  "+ApplicationPropertiesValues.APPLICATION_MODE);
	
			 if(ApplicationPropertiesValues.APPLICATION_MODE != null && 
					   !"".equals(ApplicationPropertiesValues.APPLICATION_MODE.trim()) &&
					   "prod".equals(ApplicationPropertiesValues.APPLICATION_MODE)){
				   
				  System.setProperty("log4j.configurationFile", "log4j2-prod.xml");
				  
				  
				  LoggingUtils.logInfo(".................................................................log4j2-prod.xml");
					   
				  LoggingUtils.logInfo("property file in production ...."); 
				  
				  ApplicationPropertiesValues.DB_URL =  properties.getProperty(ApplicationPropertiesKeys.DB_URL_PROD);
				  ApplicationPropertiesValues.DB_USR =  properties.getProperty(ApplicationPropertiesKeys.DB_USR_PROD); 
				  ApplicationPropertiesValues.DB_PWD =  properties.getProperty(ApplicationPropertiesKeys.DB_PWD_PROD);
				  
				  ApplicationPropertiesValues.QUEUE_NAME =  properties.getProperty(ApplicationPropertiesKeys.QUEUE_NAME_PROD);		
				  
			 }else{
				  System.setProperty("log4j.configurationFile", "log4j2-dev.xml");
				  
				  
				  LoggingUtils.logInfo(".................................................................log4j2-dev.xml");
					   
				  LoggingUtils.logInfo("property file in dev - local  ...."); 
				  
				  ApplicationPropertiesValues.DB_URL =  properties.getProperty(ApplicationPropertiesKeys.DB_URL_DEV);
				  ApplicationPropertiesValues.DB_USR =  properties.getProperty(ApplicationPropertiesKeys.DB_USR_DEV); 
				  ApplicationPropertiesValues.DB_PWD =  properties.getProperty(ApplicationPropertiesKeys.DB_PWD_DEV);
				  
				  ApplicationPropertiesValues.QUEUE_NAME =  properties.getProperty(ApplicationPropertiesKeys.QUEUE_NAME_DEV);
			   }
			 
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
