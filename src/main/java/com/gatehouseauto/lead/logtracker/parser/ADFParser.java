package com.gatehouseauto.lead.logtracker.parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.gatehouseauto.lead.logtracker.bean.Adf;
import com.gatehouseauto.lead.logtracker.utils.LoggingUtils;

public class ADFParser {
	
	private ObjectMapper objectMapper = new XmlMapper();

    public Adf parse(String adfXML) {
    	Adf adf = null;
    	try{            
            adf = objectMapper.readValue(adfXML, Adf.class);
    	}catch(Exception e){
    		LoggingUtils.logError(e==null?"Null Exception from Parser":e.getMessage());
    	}    	
    	return adf;
    }
}

